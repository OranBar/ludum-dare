﻿using UnityEngine;
using System.Collections;
using tutto;

public class FreezeTouch : ThrowableItem {

	public GameObject ice;
	private float slideSpeed;

	public override void Start(){
		base.Start();
		slideSpeed = GameObject.FindObjectOfType<PermanentSlide>().slideSpeed;
	}




	protected override void OnEnemyCollision (Collider2D enemy) {
		EnemyAI enemyAiScript = enemy.GetComponent<EnemyAI>();
		enemyAiScript.FuzzyMovement(0f, 50f);
		
		GameObject iceObj = Instantiate (ice, enemy.transform.position, Quaternion.identity) as GameObject;
		enemy.gameObject.AddComponent<PermanentSlide>();
		enemy.GetComponent<PermanentSlide>().slideSpeed = slideSpeed;
		enemy.GetComponent<CircleCollider2D>().enabled = true;
		enemy.GetComponent<BoxCollider2D>().enabled = false;

		iceObj.transform.parent = enemy.transform;
		iceObj.transform.localPosition.Set(0f, 0f, 0f);
		enemy.GetComponent<Animator>().enabled = false;

		enemyAiScript.InterruptStandardBehaviour=true;
		enemyAiScript.CanKillPlayer = false;
		Destroy(gameObject, 0.1f);
	}
}
