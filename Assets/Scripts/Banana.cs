﻿using UnityEngine;
using System.Collections;
using tutto;

public class Banana : ThrowableItem {

	public float slideSpeed = 5.0f;
	public float slidingTime = 1.2f;

	protected override void OnEnemyCollision(Collider2D enemy) {
		Vector2 slideDirection = Vector2.zero;
		Edge closerEdge = GetCloserEdge();

		switch(closerEdge) {
			case Edge.Left:
				slideDirection = (-1) * Vector2.right;
				break;

			case Edge.Right:
				slideDirection = Vector2.right;
				break;
		}
		EnemyAI enemyAi = enemy.GetComponent<EnemyAI>();

		enemyAi.InterruptStandardBehaviour = true;
		SlideEnemy (enemy, slideDirection);
		enemyAi.IsSliding = true;

		//Functional method: Not working
//		StartCoroutine( RecoverFromSlide(enemyAi) );

		Destroy(gameObject, destroyDelay);
	}

	private void SlideEnemy (Collider2D enemy, Vector2 slideDirection) {
		enemy.rigidbody2D.velocity = Vector2.zero;
		enemy.rigidbody2D.AddForce (slideDirection * slideSpeed);
		Vector3 enemyEulerAngles = enemy.transform.eulerAngles;
		if(slideDirection.x <0){
			enemyEulerAngles.z = 90f;
		} else {
			enemyEulerAngles.z = -90f;
		}
		enemy.transform.eulerAngles = enemyEulerAngles;

//		enemy.GetComponent<EnemyAI>().IsSliding = true;
	}

	private void SlideRecovery(GameObject go) { 
		Debug.Log("I am slideRecovery. My argument is : "+go.ToString());
		go.rigidbody2D.velocity = Vector2.zero;
		EnemyAI enemyAi = go.GetComponent<EnemyAI>();
		go.rigidbody2D.AddForce(Vector2.up * enemyAi.speed);

		enemyAi.InterruptStandardBehaviour = false;
	}

	//Not working
	private IEnumerator RecoverFromSlide(EnemyAI enemyAi){
		yield return new WaitForSeconds(slidingTime);
		enemyAi.ExecuteBehaviour( new EnemyAI.EnemyBehaviour(SlideRecovery) );
	}

}

