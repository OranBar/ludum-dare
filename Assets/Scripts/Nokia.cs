﻿using UnityEngine;
using System.Collections;

public class Nokia : ThrowableItem {

	public GameObject explosion;

	protected override void OnEnemyCollision (Collider2D enemy) {
		Destroy(enemy.gameObject, 0.1f);
		Destroy(gameObject, 0.2f);
		Explosion();
	}

	private void Explosion(){
		GameObject createdExplosion =  Instantiate(explosion, this.transform.position, Quaternion.identity) as GameObject;
	//	createdExplosion.animation.Play();
		Destroy(createdExplosion, 0.2f);
	}
}
