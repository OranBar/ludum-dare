﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class Spawner : MonoBehaviour {
	
	public int copsPerWave = 1;
	public float timeBeforeStart = 2.0f;
	public float timeBetweenWaves = 3.0f;
	public Transform spawnParent;
	

	public void Start() {
		Init ();
		InvokeRepeating("SpawnObj", timeBeforeStart, timeBetweenWaves );
	}
	
	protected virtual void Init () {
		if(spawnParent==null){
			spawnParent = GameObject.Find("Enemies").transform;
		}
	}
	
	private void SpawnObj() {
		List<Vector3> spawns = GetSpawnsPlaces();
		for(int i=0; i<copsPerWave; i++){
			Vector3 currentSpawn = spawns[i%(spawns.Count)];
			GameObject newObj = Instantiate(GetSpawnObject(), currentSpawn, Quaternion.identity) as GameObject;
			newObj.transform.parent = spawnParent;
			newObj.name = newObj.name.Replace("(Clone)","");
		}
	}
	
	public void StopSpawning(){
		CancelInvoke();
	}

	protected abstract GameObject GetSpawnObject();
	protected abstract List<Vector3> GetSpawnsPlaces();

	
}
