﻿using UnityEngine;
using System.Collections;

//DontUse. Not working
public class SpawnsCreator : MonoBehaviour {

	public Transform startingSpawnPoint;
	public Transform endingSpawnPoint;
	public float spawnsDistance = 20f;

	void Start () {
		/*
		float spawnAreaLength = (startingSpawnPoint.position - endingSpawnPoint.position).magnitude;
		Vector3 currentSpawn = Vector3.zero;
		for(int i=0; i<spawnAreaLength/spawnsDistance; i++){
			currentSpawn = startingSpawnPoint.position + ((spawnsDistance * i) * (endingSpawnPoint.position - startingSpawnPoint.position).normalized);
			GameObject newObj = Instantiate(new GameObject(), currentSpawn, Quaternion.identity) as GameObject;
			newObj.transform.parent = this.transform;
		}
		*/
		HorizontalSpawner();
	}

	void HorizontalSpawner(){
		Vector3 currentSpawn = Vector3.zero;
		int i=0;
		while(Vector3.Distance(currentSpawn, startingSpawnPoint.position) <= Vector3.Distance(currentSpawn, endingSpawnPoint.position)){
			currentSpawn = currentSpawn + new Vector3(spawnsDistance, 0f, 0f);
			GameObject newObj = Instantiate(new GameObject(), currentSpawn, Quaternion.identity) as GameObject;
			newObj.transform.parent = this.transform;
			i++;
			if(i>=1000){
				Debug.Log("Emergency Exit");
				break;
			}
		}
	}
	

}
