﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemySpawner : Spawner {

	public GameObject CopEnemy;

	private List<Transform> possibleSpawns;
	private int spawnsMaxIndex = 0; 

	protected override void Init () {
		base.Init();
		possibleSpawns = new List<Transform>();
		foreach (Transform currentSpawn in this.transform) {
			possibleSpawns.Add (currentSpawn);
			spawnsMaxIndex++;
		}
	}

	protected override GameObject GetSpawnObject() {
		return CopEnemy;
	}

	protected override List<Vector3> GetSpawnsPlaces (){
		List<Vector3> spawns = new List<Vector3>();
		for(int i=0; i<copsPerWave; i++){
			int randomNumber = Random.Range(0, spawnsMaxIndex);
			spawns.Add(possibleSpawns[randomNumber].position);
		}
		return spawns;
	}

}
