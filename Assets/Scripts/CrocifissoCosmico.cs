﻿using UnityEngine;
using System.Collections;

public class CrocifissoCosmico : ImmediateActivationItem {

	protected override void OnEnemyCollision (Collider2D enemy){

	}

	public override void Activate(){

		StartAnimation ();

	}

	private void StartAnimation ()
	{
//		GameManager.GetGameManager().ActivatePermaSlides(false);

		GetComponent<Animator> ().enabled = true;
		GetComponent<SpriteRenderer> ().enabled = true;
	
		MoveToCameraCenter ();
		GetComponent<Animator> ().SetTrigger ("JesusShockwave");
	}

	public void ActivateItem ()
	{
//		GameManager.GetGameManager().ActivatePermaSlides(true);
		GameObject enemiesParent = GameObject.Find ("Enemies");
		foreach (Transform currentEnemy in enemiesParent.transform) {
			Destroy (currentEnemy.gameObject);
			Destroy (gameObject, destroyDelay);
		}
	}

	private void MoveToCameraCenter(){
		this.transform.parent = null;
		this.transform.parent = Camera.main.transform;
		this.transform.localPosition = new Vector3(0f, 0f, this.transform.position.z);
	}
}
