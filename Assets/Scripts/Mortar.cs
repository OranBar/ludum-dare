﻿using UnityEngine;
using System.Collections;

public class Mortar : PrecisionFallItem {

	protected override void OnEnemyCollision (Collider2D enemy) {
		Destroy(enemy.gameObject, 0.1f);
		Destroy(gameObject, destroyDelay);
		Explosion();
	}

	private void Explosion(){
		//TODO:
	}


}
