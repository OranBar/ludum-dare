﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Score : MonoBehaviour {

	public int score;
	public Text scoreText;

	public void UpdateScore(int addToScore){
		score += addToScore;
		if(scoreText!=null){
			scoreText.text = "Score: "+score;
		}
	}
}
