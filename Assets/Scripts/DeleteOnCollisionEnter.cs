﻿using UnityEngine;
using System.Collections;

public class DeleteOnCollisionEnter : MonoBehaviour {

	public float destroyDelay = 0.2f;
	
	void OnCollisionEnter2D(Collision2D other){
		Destroy(other.gameObject, destroyDelay);
	}
}

