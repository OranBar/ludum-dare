﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(CircleCollider2D))]
public class ObjectRadius : MonoBehaviour {

	public List<GameObject> objectsInRadius = new List<GameObject>();

	public void Awake(){
		GetComponent<CircleCollider2D>().isTrigger = true;
		GetComponent<CircleCollider2D>().enabled = false;

	}

	public List<GameObject> GetGameObjectsInRadius(){
		GetComponent<CircleCollider2D>().enabled = true;
		StartCoroutine(DeactivateTrigger(0.1f));
		return objectsInRadius;
	}

	private IEnumerator DeactivateTrigger(float waitTime){
		yield return new WaitForSeconds(waitTime);
		GetComponent<CircleCollider2D>().enabled = false;

	}

	public void OnTriggerEnter2D(Collider2D other){
		objectsInRadius.Add(other.gameObject);
	}

	public void OnTriggerExit2D(Collider2D other){
		objectsInRadius.Remove(other.gameObject);
	}



}
