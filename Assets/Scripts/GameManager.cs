﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

	//Singleton
	private static GameManager gameManager;
	public static GameManager GetGameManager(){
		return gameManager;
	}

	public GameObject gameOverScreen;

	private EnemySpawner enemySpawner;
	private ItemSpawner itemSpawner;
	private GameObject player;

	void Awake(){
		Time.timeScale = 1f;
		gameManager = this;
		enemySpawner = GameObject.FindObjectOfType(typeof(EnemySpawner)) as EnemySpawner;
		itemSpawner = GameObject.FindObjectOfType(typeof(ItemSpawner)) as ItemSpawner;
		player = GameObject.FindGameObjectWithTag("Player");
	}

	public void GameOver(){
		enemySpawner.StopSpawning();
		itemSpawner.StopSpawning();
		ActivatePermaSlides(false);
		ShowGameOverScreen();
	}

	private void ShowGameOverScreen(){
		//TODO
		gameOverScreen.SetActive(true);
		Time.timeScale = 0f;
		Invoke("Reload", 3.5f);
	}

	public void Reload(){
		Time.timeScale = 1f;
		Application.LoadLevel(Application.loadedLevel);
	}

	public void Update(){
		if(player==null){
			GameOver();
		}
	}

	public void OnTriggerEnter2D(Collider2D other){
		if(other.tag == "Player"){
			ShowWinScreen();
		}
	}

	private void ShowWinScreen(){
		enemySpawner.StopSpawning();
		itemSpawner.StopSpawning();

		ActivatePermaSlides (false);
		Time.timeScale = 0f;

		Application.LoadLevel("VictoryScene");

	}

	public void ActivatePermaSlides (bool enable) {
		PermanentSlide[] permaSlideScripts = GameObject.FindObjectsOfType<PermanentSlide> ();
		foreach (PermanentSlide current in permaSlideScripts) {
			current.enabled = enable;
		}
	}
}
