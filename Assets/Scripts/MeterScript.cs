﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MeterScript : MonoBehaviour {

	public Transform startingPoint;
	public GameObject textObj;
	public int iterations;

	private GameObject canvas;

	public void Start(){
		canvas = GameObject.Find("Canvas");
		Vector3 textOffsets = new Vector3(0f,10f,0f);
		for(int i=0; i<=iterations; i++){
			GameObject text = Instantiate(textObj, startingPoint.position + i*textOffsets, Quaternion.identity) as GameObject;
			text.GetComponent<Text>().text = (i * 10) +" m";
			text.SetActive(true);
			text.transform.SetParent(canvas.transform);
		}
	}
}
