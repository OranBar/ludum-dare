﻿using UnityEngine;
using System.Collections;

namespace tutto
{
	public class EnemyAI : MonoBehaviour {

		public float speed = 140f;
		public float destroyTime = 10f;
		public float slideRecoveryTime = 1.0f; 

		public delegate void EnemyBehaviour(GameObject go); 

		public bool InterruptStandardBehaviour{get;set;}
		private bool _isSliding;
		public bool IsSliding {
			get{ return _isSliding; }
			set{
				_isSliding = value;
				if(_isSliding==true){
		//			Invoke("RecoverFromSlide", slideRecoveryTime);
				}
			}
		}
		public bool CanKillPlayer{get;set;}

		private bool closeToPlayer;
		private Transform player;
		private int playerProximityLayer;
		private Animator animator;



		void Awake() {
			this.tag = "Enemy";
			player = GameObject.FindWithTag("Player").transform;
			playerProximityLayer = LayerMask.NameToLayer("PlayerProximity");
			if(playerProximityLayer<=-1){
				Debug.LogError("Create \"PlayerProximity\" layer");
			}
			CanKillPlayer = true;
			animator = GetComponent<Animator>();
		}

		void Start() {
			Invoke("DestroyOnInvisible", 2.0f);
			rigidbody2D.AddForce(Vector2.up * speed);
		}

		void OnCollisionEnter2D(Collision2D other) {
			if(other.gameObject.tag == "Player" && CanKillPlayer){
				GameManager.GetGameManager().GameOver();
			}
		}

		public void OnTriggerEnter2D(Collider2D other){
			if(other.gameObject.layer == playerProximityLayer){
				closeToPlayer = true;
			}
		}

		public void OnTriggerExit2D(Collider2D other){
			if(other.gameObject.layer == playerProximityLayer){
				closeToPlayer = false;
			}
		}

		public void FixedUpdate(){
			if(InterruptStandardBehaviour || IsSliding){
				return;
			}

			if(closeToPlayer){
				ConvergeOntoPlayer();
			}
		}

		private bool CanMainCameraSeeMe() {
			Vector3 enemyViewPort = Camera.main.WorldToViewportPoint((transform.position));
			return (enemyViewPort.x <0f || enemyViewPort.y>1f || enemyViewPort.y<0f || enemyViewPort.y>1f);
		}

		private void DestroyOnInvisible(){
			StartCoroutine( DestroyOnInvisible_Coro() );
		}

		private IEnumerator DestroyOnInvisible_Coro(){
			while(true){
				if(CanMainCameraSeeMe()){
					Destroy(gameObject, 0.1f);
				}
				yield return null;
			}
		}

		private void ConvergeOntoPlayer(){
			rigidbody2D.velocity = Vector2.zero;
			if(player==null){
				return;
			}
			Vector3 enemyToPlayerVec = player.transform.position - transform.position;

			TriggerMovementAnimation(enemyToPlayerVec);

			rigidbody2D.AddForce( ((player.transform.position - transform.position).normalized) * speed * 0.70f); //Magic number slows down a bit
		}

		private void TriggerMovementAnimation(Vector3 movementVector){
			if( Mathf.Abs(movementVector.x) >= Mathf.Abs(movementVector.y) ){
				//Moving horizontally
				if(movementVector.x>0){
					animator.SetTrigger("Right");
				} 
				if(movementVector.x<0){
					animator.SetTrigger("Left");
				}
			} else {
				//Moving vertically
				if(movementVector.y<0) {
					animator.SetTrigger("Down");
				}
				if(movementVector.y>0){
					animator.SetTrigger ("Up");
				}
			} 
		}

		public void ExecuteBehaviour(EnemyBehaviour behaviour){
			behaviour(this.gameObject);
		}

		private void RecoverFromSlide() { 
			rigidbody2D.velocity = Vector2.zero;
			rigidbody2D.AddForce(Vector2.up * speed);
			IsSliding = false;
		}

		public void FuzzyMovement(float fuzzyness, float timeBetweenDirectionChanges){
			StartCoroutine( RandomFuzzyMovement(fuzzyness, timeBetweenDirectionChanges) );
		}

		private IEnumerator RandomFuzzyMovement(float force, float timeBetweenDirectionChanges){
			while(true){
				rigidbody2D.velocity = Vector2.zero;
				float randomX = Random.Range(-1f,1f);
				float randomY = Random.Range(-1f,1f);
				Vector3 fuzzyDirection = new Vector3(randomX, randomY, 0f);
				rigidbody2D.AddForce(fuzzyDirection * force);
				yield return new WaitForSeconds(timeBetweenDirectionChanges);
			}
		}

		public void OnDestroy(){
			Score score = GameObject.FindObjectOfType<Score>();
			if(score!=null){
				GameObject.FindObjectOfType<Score>().UpdateScore(Random.Range(34,51));
			}
		}
	}
}
