﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[ExecuteInEditMode()]
public class ItemSpawner : Spawner {

	public List<GameObject> items = new List<GameObject>();
	public List<int> spawnChance = new List<int>(); 

	private float middleClipPlame;

	[ExecuteInEditMode()]
	public void Update(){
#if UNITY_EDITOR
		while(spawnChance.Count < items.Count){
			spawnChance.Add(0);
		}
#endif
	}

	protected override void Init () {
		base.Init ();
		middleClipPlame = Camera.main.farClipPlane/2;
	}

	protected override GameObject GetSpawnObject ()	{
//		return GetRandomObject();
		return GetObjectUsingSpawnChance();
	}

	private GameObject GetRandomObject(){
		int randomNumber = Random.Range(0, items.Count);
		return items[randomNumber];
	}

	private GameObject GetObjectUsingSpawnChance(){
		int randomNumber = Random.Range(0,101);
		GameObject obj = null;
		int currentChance = 0;
		for(int i=0; i<spawnChance.Count; i++){
			currentChance = currentChance + spawnChance[i];
			if(currentChance >= randomNumber){

				obj = items[i];
				break;
			}
		}

		if(obj==null){
			Debug.LogError("Couldn't select any Item. % are wrong");
		}
		return obj;
	}


	/*

	public List<Transform> possibleSpawns = new List<Transform>();

	protected override List<Transform> GetSpawnsPlaces () {
		List<Transform> spawn = new List<Transform>();
		int randomNumber = Random.Range(0, possibleSpawns.Count-1);
		spawn.Add(possibleSpawns[randomNumber]);
		return spawn;
	}
*/
	protected override List<Vector3> GetSpawnsPlaces () {
		Vector3 screenPosition = Camera.main.ScreenToWorldPoint(new Vector3(Random.Range(Screen.width/5,(Screen.width/5)*4), Random.Range(Screen.height/5,Screen.height), middleClipPlame));
		List<Vector3> spawn = new List<Vector3>();
		spawn.Add(screenPosition);
		return spawn;
	}
}
