﻿using UnityEngine;
using System.Collections;

public class LevelLoader : MonoBehaviour {

	public void Awake(){
		DontDestroyOnLoad(this.gameObject);
	}

	public void LoadLevel(string levelName){
		Application.LoadLevel(levelName);
	}

	public void LoadLevel(int levelId){
		Application.LoadLevel(levelId);
	}
}
