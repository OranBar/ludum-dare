﻿using UnityEngine;
using System.Collections;

public class ItemThrower : MonoBehaviour {

	public float throwSpeed = 5f;
	public float throwPrecision = 0.1f;

	private Inventory inventory;
	private Transform player;

	public void Start() {
		player = GameObject.FindGameObjectWithTag("Player").transform;
		inventory = player.GetComponent<Inventory>();
	}

	void Update() {
		if(Input.GetMouseButtonDown(0) && inventory.HasItem()) {
			ThrowItem();
			inventory.ClearItem();
		}
	}

	private void ThrowItem() {
		Vector3 throwTarget = Camera.main.ScreenToWorldPoint(Input.mousePosition);
		Item item = inventory.GetItem();
		Physics2D.IgnoreCollision(item.collider2D, player.collider2D);
		item.transform.position = player.transform.position;
		item.gameObject.SetActive(true);
		item.rigidbody2D.AddForce( ((throwTarget - player.transform.position) * throwSpeed));


	//	StartCoroutine( Move(item, throwTarget) );
	}
	/*
	IEnumerator Move(GameObject item, Vector3 target) {
		target.z = 0;
		while(item !=null && Vector3.Distance(item.transform.position, target) >= throwPrecision ){
			item.transform.position = Vector3.Lerp(item.transform.position, target, throwSpeed * Time.deltaTime);
			yield return null;
		}
		if(item !=null){
			Physics2D.IgnoreCollision(item.collider2D, player.collider2D, false);
		}
	}
	*/

}
