﻿using UnityEngine;
using System.Collections;

public class PlayerMovement2D : MonoBehaviour {

	public float speed = 2300;

	private Animator animator;

	void Awake() {
		this.tag = "Player";
		animator = GetComponent<Animator>();
	}

	void FixedUpdate() {
		float horizontal = Input.GetAxisRaw("Horizontal");
		float vertical = Input.GetAxisRaw("Vertical");
		if (MovingBackwards (vertical)) {
				vertical *= 1.60f;
		}
		rigidbody2D.AddForce(new Vector2(horizontal, vertical) * speed * Time.deltaTime);

		TriggerMovementAnimation(horizontal, vertical);

		rigidbody2D.angularVelocity = 0; //Boh?

		Vector3 currentEulerAngles = transform.eulerAngles;
		currentEulerAngles.z = 0f;
		this.transform.eulerAngles = currentEulerAngles;
	}

	private void TriggerMovementAnimation(float horizontal, float vertical){
		if( Mathf.Abs(horizontal) >= Mathf.Abs(vertical) ){
			//Moving horizontally
			if(horizontal>0){
				animator.SetTrigger("Right");
			} 
			if(horizontal<0){
				animator.SetTrigger("Left");
			}
		} else {
			//Moving vertically
			if(vertical<0) {
				animator.SetTrigger("Down");
			}
			if(vertical>0){
				animator.SetTrigger ("Up");
			}
		} 
	}

	private bool MovingBackwards (float verticalAxisInput){
		return verticalAxisInput < 0;
	}

	/*
	void OnCollisionEnter2D(Collision2D other){
		if(other.gameObject.layer == LayerMask.NameToLayer("Borders")){
			Debug.Log("mehere");
			GameManager.GetGameManager().GameOver();
			Destroy(this.gameObject);
		}
	}
	*/
}
