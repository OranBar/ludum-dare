﻿using UnityEngine;
using System.Collections;

public class BackgroundSlide : MonoBehaviour {


	public GameObject background;
	public float slideSpeed = 1f;

	void OnTriggerStay2D(Collider2D other) {
		if(other.tag == "Player"){
			background.transform.Translate(new Vector3(0f, (-1f)*slideSpeed, 0f));
		}
	}
}
