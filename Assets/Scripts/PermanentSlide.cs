﻿using UnityEngine;
using System.Collections;

public class PermanentSlide : MonoBehaviour {
		
	public float slideSpeed = 1f;
	
	void Update() {
		transform.Translate(new Vector3(0f, (-1f)*slideSpeed, 0f));
	}
}


