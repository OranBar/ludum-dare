﻿using UnityEngine;
using System.Collections;

public class DeleteOnTriggerEnter : MonoBehaviour {

	public float destroyDelay = 0.2f;

	void OnTriggerEnter2D(Collider2D other){
		Destroy(other.gameObject, destroyDelay);
	//	GameObject.FindObjectOfType<Score>().UpdateScore(Random.Range(0,51) );
	}
}
