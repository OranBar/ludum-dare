﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Inventory : MonoBehaviour {

	public Text itemText;
	public Image itemImage;

	public Item currentItem;
	public float throwSpeed = 5f;
	public float precisionBombDelay = 0.7f;
	public GameObject xMark;

	private GameObject player;


	public void Awake(){
		player = GameObject.FindGameObjectWithTag("Player");
	}

	void OnTriggerEnter2D(Collider2D other) {
		if(currentItem==null && other.tag == "Items"){
			currentItem = other.GetComponent<Item>();

			UpdateItemPanel(currentItem);


	//		other.gameObject.SetActive(false);
			other.GetComponent<BoxCollider2D>().enabled = false;
			other.GetComponent<SpriteRenderer>().enabled = false;
		}
	}

	private void UpdateItemPanel(Item item){
		Color opaque = Color.white;
		opaque.a = 1f;
		itemImage.color = opaque;
		itemText.text = item.name;
		itemImage.sprite = item.GetComponent<SpriteRenderer>().sprite;

	}

	public void Update() {
		if(Input.GetMouseButtonDown(0) && HasItem()) {
			Vector3 target = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			UseItem(currentItem, target);
			ClearItem();
		}
	}

	public bool HasItem(){
		if(currentItem==null){
			return false;
		} else {
			return true;
		}
	}

	public Item GetItem(){
		return currentItem;
	}

	public void UseItem(Item item, Vector3 target){
		item.transform.parent = null;
		if(item is ThrowableItem){
			ThrowItem();
		}
		else if(item is PrecisionFallItem){
			StartCoroutine( PrecisionBombimgThrowItem() );
		}
		else if(item is ImmediateActivationItem){
			ImmediateActivationItem castedItem = (ImmediateActivationItem) item;
			castedItem.Activate();
		}

	}

	public void ClearItem() {
		currentItem = null;
		itemText.text = "";
		itemImage.sprite = null;
		Color transparent = Color.white;
		transparent.a = 0f;
		itemImage.color = transparent;
	}

	private void ThrowItem() {
		Vector3 throwTarget = Camera.main.ScreenToWorldPoint(Input.mousePosition);
		Item item = GetItem();
		Physics2D.IgnoreCollision(item.collider2D, player.collider2D);
		item.transform.position = player.transform.position;
		item.GetComponent<SpriteRenderer>().enabled = true;
		item.GetComponent<BoxCollider2D>().enabled = true;
//		item.gameObject.SetActive(true);
		item.rigidbody2D.AddForce( ((throwTarget - player.transform.position) * throwSpeed));
	}

	private IEnumerator PrecisionBombimgThrowItem(){
		Vector3 throwTarget = Camera.main.ScreenToWorldPoint(Input.mousePosition);
		throwTarget.z += 1.1f;
		Item item = GetItem();
		Physics2D.IgnoreCollision(item.collider2D, player.collider2D);
		item.transform.position = throwTarget;


		InstantiateXMarker(throwTarget, item);

		yield return new WaitForSeconds(precisionBombDelay);

		item.GetComponent<SpriteRenderer>().enabled = true;
		item.GetComponent<BoxCollider2D>().enabled = true;
		foreach(Transform child in item.transform){
			Destroy(child.gameObject);
		}
		Destroy(item.gameObject, 0.3f);

	//	item.gameObject.SetActive(true);
		//TODO: ANIMATION

	}

	private void InstantiateXMarker(Vector3 throwTarget, Item item){
		throwTarget.z += 1f; 
		GameObject mark = Instantiate(xMark, throwTarget, Quaternion.identity) as GameObject;
		mark.transform.parent = item.transform;
	}


}	
