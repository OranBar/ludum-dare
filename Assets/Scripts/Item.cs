﻿using UnityEngine;
using System.Collections;

public enum Edge{
	Left, Right,
}

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(BoxCollider2D))]
public abstract class Item : MonoBehaviour {

	public float destroyDelay = 0.2f;

	public virtual void Start() {
		this.tag = "Items";
		this.gameObject.layer = LayerMask.NameToLayer("Items");
		if(this.gameObject.layer <= -1){
			Debug.LogError("Create \"Items\" layer");
		}
		rigidbody2D.gravityScale = 0f;
		GetComponent<BoxCollider2D>().isTrigger = true;
	}
	
	public virtual void OnTriggerEnter2D(Collider2D other) {
		if(other.tag == "Enemy"){
			OnEnemyCollision(other);
	//		GameObject.FindObjectOfType<Score>().UpdateScore(Random.Range(0,51) );
		}
	}
	
	protected Edge GetCloserEdge() {
		if(this.transform.position.x >= 0.0f){
			return Edge.Right;
		} else {
			return Edge.Left;
		}
	}
	
	protected abstract void OnEnemyCollision(Collider2D enemy);

}
