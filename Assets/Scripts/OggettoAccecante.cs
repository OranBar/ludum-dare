﻿using UnityEngine;
using System.Collections;
using tutto;

public class OggettoAccecante : ItemWithRadiusEffect {

	public float forces = 80f;
	public float timeBetweenMovementChange = 0.7f;
	public GameObject stars;


	protected override void OnEnemyCollision (Collider2D enemy) {
		GetComponent<Animator>().SetTrigger("Light");
		EnemyAI enemyAiScript = enemy.GetComponent<EnemyAI>();
		enemyAiScript.FuzzyMovement(forces, timeBetweenMovementChange);

		GameObject starsObj = Instantiate(stars, enemy.transform.position, Quaternion.identity) as GameObject;
		starsObj.transform.parent = enemy.transform;
		starsObj.transform.localPosition = new Vector3(0f, 0.15f, 0f);
		starsObj.animation.Play();
		enemy.renderer.material.color = Color.gray;

		enemyAiScript.InterruptStandardBehaviour = true;

		Destroy(gameObject, 0.1f);
	}
	


}
