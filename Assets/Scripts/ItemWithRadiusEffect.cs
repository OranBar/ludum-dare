﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(ObjectRadius))]
public abstract class ItemWithRadiusEffect : ThrowableItem {

	private ObjectRadius objectRadius;

	public void Awake(){
		objectRadius = GetComponent<ObjectRadius>();
	}

	public List<GameObject> GetGameObjectsInRadius(){
		return objectRadius.GetGameObjectsInRadius();
	}

	public override void OnTriggerEnter2D (Collider2D other) {
		if(other.tag == "Enemy"){
			List<GameObject> objectsInRadius = GetGameObjectsInRadius();
			foreach(GameObject closeObj in objectsInRadius){
				if(closeObj!=null && closeObj.tag == "Enemy"){
					OnEnemyCollision(closeObj.collider2D);
				}
			}
		}
	}
}
