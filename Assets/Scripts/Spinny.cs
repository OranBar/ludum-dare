﻿using UnityEngine;
using System.Collections;

public class Spinny : MonoBehaviour {

	public float spinSpeed = 2.0f;

	private bool spin = false;
	private Vector3 previousLocalPosition;

	public void Start(){
		previousLocalPosition = this.transform.localPosition;
		spin = false;
	}

	public void FixedUpdate () {
		Vector3 currentLocalPosition = this.transform.localPosition;
		if(currentLocalPosition != previousLocalPosition){
			spin=true;
		}
		previousLocalPosition = currentLocalPosition;

		if(spin==true){
			Vector3 currentEulerAngles = this.transform.eulerAngles;
			currentEulerAngles.z -= spinSpeed;
			this.transform.eulerAngles = currentEulerAngles;
		}

	}


}
